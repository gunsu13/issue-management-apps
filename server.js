const http = require('http');
const app = require('./app');
const conf = require('./config');

// menentukan port
const port = conf.port;

const server = http.createServer(app);

// listening ke port dan request http yang sudah ditentukan
server.listen(port, () => console.log(`listeing on port ${port}`));