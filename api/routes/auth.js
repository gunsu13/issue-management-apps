const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt')
const { check } = require('express-validator')
const jwt = require('jsonwebtoken')

const config = require('../../config')

const User = require('../models/user')


router.post('/login', async(req, res) => {

    if (!(req.body.username && req.body.password)) {
        res.status(400).send("All input is required");
    }

    const user = await User.findOne({ username: req.body.username })

    if (user) {
        const validPassword = await bcrypt.compare(req.body.password, user.password);
        if (validPassword) {

            const token = jwt.sign({ id: user._id, username: user.username }, config.secret, {
                expiresIn: 86400 // expires in 24 hours
            });

            // save user token
            user.token = token;

            res.status(200).json({
                status: true,
                message: "User is successfuly authenticated",
                token: token,
                userData: {
                    _id: user._id,
                    display_name: user.display_name,
                    username: user.username,
                    isAdmin: user.isAdmin
                }
            });
        } else {
            res.status(400).json({
                status: false,
                message: "Invalid Password"
            });
        }
    } else {
        res.status(401).json({
            status: false,
            message: "User does not exist"
        });
    }
})

module.exports = router;