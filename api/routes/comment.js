const express = require('express')
const router = express.Router()

const User = require('../models/user')
const Comment = require('../models/comment')

const mongoose = require('mongoose')

router.get('/list', async(req, res, next) => {
    if (!req.query.issue_id) {
        res.status(400).send({
            status: true,
            message: 'params is not set correctly'
        })
        return
    }

    const currentPage = (req.query.page) ? req.query.page : 1;
    const size = (req.query.size) ? req.query.size : 5;
    const offset = (req.query.page > 1) ? (currentPage * size) - size : 0;

    const totalItems = await Comment.find({
        issue: mongoose.Types.ObjectId(req.query.issue_id),
        deletedAt: null
    }).count()

    Comment.find({
            issue: mongoose.Types.ObjectId(req.query.issue_id),
            deletedAt: null
        })
        .sort({ createdAt: -1 })
        .populate('issue', '_id issue_code')
        .populate('user', 'display_name username')
        .limit(size)
        .skip(offset)
        .exec()
        .then(result => {
            if (result.length > 0) {
                res.status(200).send({
                    status: true,
                    message: "data found",
                    currentPage: parseInt(currentPage),
                    totalItems: totalItems,
                    totalPages: Math.ceil(totalItems / size),
                    data: result.map(dt => {
                        return {
                            _id: dt._id,
                            issue: dt.issue,
                            user: dt.user.display_name,
                            comment_body: dt.comment_body,
                            createdAt: dt.createdAt,
                            updatedAt: dt.updatedAt,
                            deletedAt: dt.deletedAt,
                            meta: {
                                urlQuery: '?comment_id=' + dt._id
                            }
                        }
                    })
                })
            } else {
                res.status(400).send({
                    status: true,
                    message: 'project lis is empty',
                    currentPage: parseInt(currentPage),
                    totalItems: totalItems,
                    totalPages: Math.ceil(totalItems / size)
                })
            }
        }).catch(err => {
            console.log(err)
            res.status(500).send({
                error: 'something wrong with server',
            })
        });
})


router.get('/stats', async(req, res, next) => {
    const count = await Comment.count({ issue: mongoose.Types.ObjectId(req.query.issue_id), deletedAt: null })

    if (count) {
        res.status(200).send({
            status: true,
            message: "data found",
            countComment: count
        })
    } else {
        res.status(400).send({
            status: true,
            message: 'project lis is empty'
        })
    }

})

router.get('/:id', async(req, res, next) => {
    Comment.findOne({
            _id: req.params.id,
            deletedAt: null
        })
        .populate('issue', '_id issue_code')
        .populate('user', 'display_name username')
        .exec()
        .then(result => {
            if (result) {
                res.status(200).send({
                    status: true,
                    message: "data found",
                    data: {
                        _id: result._id,
                        issue: result.issue,
                        user: result.user.display_name,
                        comment_body: result.comment_body,
                        createdAt: result.createdAt,
                        updatedAt: result.updatedAt,
                        deletedAt: result.deletedAt,
                        meta: {
                            urlQuery: '?comment_id=' + result._id
                        }
                    }
                })
            } else {
                res.status(400).send({
                    status: true,
                    message: 'comment not found'
                })
            }
        }).catch(err => {
            console.log(err)
            res.status(500).send({
                error: 'something wrong with server',
            })
        });
})

router.post('/', async(req, res, next) => {
    const body = req.body

    if (!(body.comment_body && body.issue_id)) {
        res.status(400).send({
            status: false,
            message: 'issue_id not found or please insert your comment'
        })
        return
    }

    const user = await User.findOne({ username: req.user.username }).select('_id username display_name')
    if (!user) {
        res.status(401).send({
            status: false,
            message: 'user not found, please relogin and get that token'
        })
        return
    }

    const comment = new Comment({
        _id: new mongoose.Types.ObjectId(),
        issue: body.issue_id,
        user: user._id,
        comment_body: body.comment_body,
        createdAt: new Date(),
        updatedAt: null,
        deletedAt: null
    })

    comment.save()
        .then(result => {
            res.status(200).json({
                status: true,
                message: "comment is successfuly added"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
})

router.patch('/:id', async(req, res, next) => {
    const body = req.body

    if (!(body.comment_body)) {
        res.status(400).send({
            status: false,
            message: 'please insert your comment'
        })
        return
    }

    Comment.findOneAndUpdate({ _id: req.params.id }, {
            comment_body: body.comment_body,
            updatedAt: new Date(),
        }, { new: true })
        .then(result => {
            res.status(200).json({
                status: true,
                message: "comment is successfuly updated",
                data_updated: result
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
})

router.delete('/:id', async(req, res, next) => {
    Comment.findOneAndUpdate({ _id: req.params.id }, {
            deletedAt: new Date(),
        }, { new: true })
        .then(result => {
            res.status(200).json({
                status: true,
                message: "comment is successfuly deleted",
                data_updated: result
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
})


module.exports = router