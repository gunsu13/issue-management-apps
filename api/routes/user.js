const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt')
const { check } = require('express-validator')
const jwt = require('jsonwebtoken')

const config = require('../../config')

// import model
const User = require('../models/user')

const mongoose = require('mongoose')
const { json } = require('express')

/**
 * this route provide all database
 */
router.get('/', (req, res, next) => {
    User.find()
        .select('_id display_name username user_type isAdmin createdAt deletedAt')
        .exec()
        .then(result => {
            res.status(200).json(result)
        }).catch(err => {
            res.status(404).json({
                error: err
            })
        });
});

/**
 * this route provide all database
 */
router.get('/:id', (req, res, next) => {
    User.findOne({ _id: req.params.id })
        .select('_id display_name username user_type isAdmin createdAt deletedAt')
        .exec()
        .then(result => {
            res.status(200).json(result)
        }).catch(err => {
            res.status(404).json({
                error: err
            })
        });
});
/**
 * this route function to store data to database
 */

router.post('/',
    check('display_name')
    .isEmpty()
    .withMessage('Display name is required'),
    check('username')
    .isEmpty()
    .withMessage('username name is required'),
    check('password')
    .isEmpty()
    .withMessage('password name is required'),

    async(req, res, next) => {
        // validate username is already taken or no
        const oldUser = await User.findOne({ username: req.body.username })
        if (oldUser) {
            res.status(400).json({
                message: 'Username already taken'
            })
            return
        }

        let isAdmin = false;
        if (req.body.isAdmin) { isAdmin = req.body.isAdmin; }

        const user = new User({
            _id: new mongoose.Types.ObjectId(),
            display_name: req.body.display_name,
            username: req.body.username,
            password: await bcrypt.hash(req.body.password, 10),
            isAdmin: isAdmin,
            token: null,
            createdAt: new Date(),
            deletedAt: null
        });

        user.save()
            .then(result => {

                const token = jwt.sign({ id: user._id, username: user.username }, config.secret, {
                    expiresIn: 86400 // expires in 24 hours
                });

                // save user token
                user.token = token;

                res.status(200).json({
                    status: true,
                    message: "user is successfuly added",
                    token: token,
                    userData: {
                        _id: user._id,
                        display_name: user.display_name,
                        username: user.username,
                        isAdmin: user.isAdmin
                    }
                });
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                    error: err
                })
            });
    });

/**
 * update user display_name and isAdmin setup
 */
router.patch('/:username',
    check('display_name')
    .isEmpty(),
    async(req, res) => {

        const user = await User.findOne({ username: req.params.username })
        if (!user) {
            res.status(400).json({
                status: false,
                message: "user " + req.body.username + " does not exist"
            });
            return
        }

        let isAdmin = user.isAdmin;
        if (req.body.isAdmin) { isAdmin = req.body.isAdmin; }

        // update password
        const doc = await User.findOneAndUpdate({ _id: user._id }, { display_name: req.body.display_name, isAdmin: isAdmin }, { new: true });

        res.status(200).json({
            status: true,
            message: "user is updated successfully",
            data: {
                display_name: doc.display_name,
                isAdmin: doc.isAdmin,
            }
        });
    })

/**
 * this route function change user password
 */
router.post('/changePassword', async(req, res, next) => {
    const body = req.body

    if (!(body.oldPassword && body.newPassword && body.confirmPassword)) {
        res.status(400).send({
            status: false,
            message: "oldPassword, newPassword and confirmPassword field is required"
        });

        return
    }

    const user = await User.findOne({ username: req.user.username })

    if (!user) {
        res.status(401).json({
            status: false,
            message: "User does not exist"
        })
        return
    }

    // compare with current password
    const validPassword = await bcrypt.compare(body.oldPassword, user.password);
    if (!validPassword) {
        res.status(400).json({
            status: false,
            message: "Invalid Password"
        });
        return
    }

    // compare new password with its confirmation
    const newPassword = await bcrypt.hash(body.newPassword, 10)

    const confirmedPassword = await bcrypt.compare(body.confirmPassword, newPassword);
    if (!confirmedPassword) {
        res.status(400).json({
            status: false,
            message: "Password confirmation doesnt match"
        });
        return
    }

    // update password
    const doc = await User.findOneAndUpdate({ _id: req.user.id }, { password: newPassword },
        // If `new` isn't true, `findOneAndUpdate()` will return the
        // document as it was _before_ it was updated.
        { new: true }
    );

    res.status(200).json({ status: true, message: "password has change", data: doc });
})

module.exports = router;