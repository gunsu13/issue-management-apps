const express = require('express')
const router = express.Router()

// import model
const Project = require('../models/project')
const User = require('../models/user')
const Issue = require('../models/issue')

const mongoose = require('mongoose')

/** view project list */
router.get('/', async(req, res, next) => {

    let objectQuery = { deletedAt: null };

    if (req.query.search) {
        const serach = { $text: { $search: req.query.search } };
        objectQuery = Object.assign(objectQuery, serach)
    }

    const currentPage = (req.query.page) ? req.query.page : 1;
    const size = (req.query.size) ? req.query.size : 5;
    const offset = (req.query.page > 1) ? (currentPage * size) - size : 0;

    const totalItems = await Project.find({ deletedAt: null }).count()

    Project.find(objectQuery)
        .sort({ createdAt: -1 })
        .limit(size)
        .skip(offset)
        .exec()
        .then(result => {
            if (result.length > 0) {
                res.status(200).send({
                    status: true,
                    message: "data found",
                    currentPage: parseInt(currentPage),
                    totalItems: totalItems,
                    totalPages: Math.ceil(totalItems / size),
                    data: result.map((dt) => {
                        return {
                            _id: dt._id,
                            project_name: dt.project_name,
                            project_description: dt.project_description,
                            project_client: dt.project_client,
                            project_status: dt.project_status,
                            createdBy: dt.createdBy,
                            updatedBy: dt.updatedBy,
                            createdAt: dt.createdAt,
                            updatedAt: dt.updatedAt,
                            deletedAt: dt.deletedAt,
                            meta: {
                                urlQuery: '?id=' + dt._id
                            }
                        }
                    })
                })
            } else {
                res.status(400).send({
                    status: true,
                    message: 'project lis is empty',
                    currentPage: parseInt(currentPage),
                    totalItems: totalItems,
                    totalPages: Math.ceil(totalItems / size)
                })
            }
        }).catch(err => {
            res.status(404).send({
                error: err
            })
        });
})

/** view project list */
router.get('/:id', async(req, res, next) => {
    const project = await Project.findOne({ _id: req.params.id });
    const projectIssueCount = await Issue.count({ project_id: mongoose.Types.ObjectId(req.params.id), deletedAt: null });

    const projectIssueCountDetil = await Issue.aggregate([{
            $match: // search or filter the field
            {
                project_id: mongoose.Types.ObjectId(req.params.id),
                deletedAt: null
            }
        },
        {
            $group: {
                _id: "$issue_status",
                count: { "$sum": 1 } // how to count each issue
            }
        }
    ])

    res.status(200).send({
        status: true,
        message: "data found",
        data: {
            project: project,
            total_issue: projectIssueCount,
            count_each_issue: projectIssueCountDetil.map((dt) => {
                return {
                    issue_status: dt._id,
                    status_count: dt.count
                }
            }),
        }
    })

})

/** insert project */
router.post('/', async(req, res, next) => {
    const body = req.body

    if (!(body.project_name && body.project_client && body.project_status)) {
        res.status(400).send({
            status: false,
            message: 'project_name, project_client, project_status are required'
        })
        return
    }

    const user = await User.findOne({ username: req.user.username }).select('_id username display_name')
    if (!user) {
        res.status(401).send({
            status: false,
            message: 'user not found, please relogin and get that token'
        })
        return
    }

    const project = new Project({
        _id: new mongoose.Types.ObjectId(),
        project_name: body.project_name,
        project_description: body.project_description,
        project_client: body.project_client,
        project_status: body.project_status,
        createdBy: user,
        updatedBy: null,
        createdAt: new Date(),
        updatedAt: null,
        deletedAt: null
    })

    project.save()
        .then(result => {
            res.status(200).json({
                status: true,
                message: "project is successfuly added"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
})

/** update project */
router.patch('/:id', async(req, res) => {
    const body = req.body

    if (!(body.project_name && body.project_client && body.project_status)) {
        res.status(400).send({
            status: false,
            message: 'project_name, project_client, project_status are required'
        })
        return
    }

    const user = await User.findOne({ username: req.user.username }).select('_id username display_name')
    if (!user) {
        res.status(401).send({
            status: false,
            message: 'user not found, please relogin and get that token'
        })
        return
    }

    const project = {
        project_name: body.project_name,
        project_description: body.project_description,
        project_client: body.project_client,
        project_status: body.project_status,
        updatedBy: user,
        updatedAt: new Date(),
    }

    const doc = await Project.findOneAndUpdate({ _id: req.params.id }, project, { new: true });

    res.status(200).json({
        status: true,
        message: "project is updated successfully",
        data: {
            project_name: doc.project_name,
            project_description: doc.project_description,
            project_client: doc.project_client,
            project_status: doc.project_status,
            updatedBy: doc.updatedBy,
            updatedAt: doc.updatedAt,
        }
    });
})

/** update project */
router.delete('/:id', async(req, res) => {

    const user = await User.findOne({ username: req.user.username }).select('_id username display_name')
    if (!user) {
        res.status(401).send({
            status: false,
            message: 'user not found, please relogin and get that token'
        })
        return
    }

    const project = {
        updatedBy: user,
        deletedAt: new Date(),
    }

    const doc = await Project.findOneAndUpdate({ _id: req.params.id }, project, { new: true });

    res.status(200).json({
        status: true,
        message: "project is deleted successfully",
        data: {
            updatedBy: doc.updatedBy,
            updatedAt: doc.updatedAt,
        }
    });
})

module.exports = router;