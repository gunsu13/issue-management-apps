const express = require('express')
const router = express.Router()

const User = require('../models/user')
const Issue = require('../models/issue')

const mongoose = require('mongoose')

router.get('/', async(req, res, next) => {

    // cek query project apakah ada jika ada maka filter query project
    let objectQuery = (req.query.project) ? { project_id: mongoose.Types.ObjectId(req.query.project), deletedAt: null } : { deletedAt: null };

    if (req.query.search) {
        const serach = { $text: { $search: req.query.search } };
        objectQuery = Object.assign(objectQuery, serach)
    }

    const currentPage = (req.query.page) ? req.query.page : 1;
    const size = (req.query.size) ? req.query.size : 5;
    const offset = (req.query.page > 1) ? (currentPage * size) - size : 0;

    const totalItems = await Issue.find(objectQuery).count()

    Issue.find(objectQuery)
        .populate('project_id', 'project_name project_description project_client project_status')
        .sort({ createdAt: -1 })
        .limit(size)
        .skip(offset)
        .exec()
        .then(result => {
            if (result.length > 0) {
                res.status(200).send({
                    status: true,
                    message: "data found",
                    currentPage: parseInt(currentPage),
                    totalItems: totalItems,
                    totalPages: Math.ceil(totalItems / size),
                    data: result.map(dt => {
                        return {
                            _id: dt._id,
                            project_id: dt.project_id,
                            issue_code: dt.issue_code,
                            issue_type: dt.issue_type,
                            issue_description: dt.issue_description,
                            issue_modul: dt.issue_modul,
                            issue_menu: dt.issue_menu,
                            issue_patch: dt.issue_patch,
                            issue_status: dt.issue_status,
                            issue_notes: dt.issue_notes,
                            createdBy: dt.createdBy,
                            updatedBy: dt.updatedBy,
                            createdAt: dt.createdAt,
                            updatedAt: dt.updatedAt,
                            deletedAt: dt.deletedAt,
                            meta: {
                                urlQuery: '?id=' + dt._id
                            }
                        }
                    })
                })
            } else {
                res.status(400).send({
                    status: true,
                    message: 'project lis is empty',
                    currentPage: parseInt(currentPage),
                    totalItems: totalItems,
                    totalPages: Math.ceil(totalItems / size)
                })
            }
        }).catch(err => {
            console.log(err)
            res.status(404).send({
                error: 'something wrong with server',
            })
        });
})

router.get('/:id', async(req, res, next) => {
    // cek query project apakah ada jika ada maka filter query project
    if (req.params.id == '') {
        res.send(404).send({
            status: false,
            message: "nothing to do here",
        })
        return;
    }

    Issue.findOne({ _id: req.params.id })
        .populate('project_id', 'project_name project_description project_client project_status')
        .exec()
        .then(result => {
            if (result) {
                res.status(200).send({
                    status: true,
                    message: "data found",
                    data: {
                        _id: result._id,
                        project_id: result.project_id,
                        issue_code: result.issue_code,
                        issue_type: result.issue_type,
                        issue_description: result.issue_description,
                        issue_modul: result.issue_modul,
                        issue_menu: result.issue_menu,
                        issue_patch: result.issue_patch,
                        issue_status: result.issue_status,
                        issue_notes: result.issue_notes,
                        createdBy: result.createdBy,
                        updatedBy: result.updatedBy,
                        createdAt: result.createdAt,
                        updatedAt: result.updatedAt,
                        deletedAt: result.deletedAt,
                        meta: {
                            urlQuery: '?id=' + result._id
                        }
                    }

                })
            } else {
                res.status(400).send({
                    status: true,
                    message: 'not founnd'
                })
            }
        }).catch(err => {
            console.log(err)
            res.status(404).send({
                error: 'something wrong with server',
            })
        });
})

router.post('/', async(req, res, next) => {
    const body = req.body
    if (!(body.project_id && body.issue_type && body.issue_description && body.issue_status)) {
        res.status(400).send({
            status: false,
            message: 'project_id, issue_type, issue_status, issue_description are required'
        })
        return
    }

    const user = await User.findOne({ username: req.user.username }).select('_id username display_name')
    if (!user) {
        res.status(401).send({
            status: false,
            message: 'user not found, please relogin and get that token'
        })
        return
    }

    const issue = new Issue({
        _id: new mongoose.Types.ObjectId(),
        project_id: body.project_id,
        issue_code: new Date().toISOString().replace('-', '/').split('T')[0].replace('-', '/') + "/" + Math.floor(Math.random() * 999),
        issue_type: body.issue_type,
        issue_description: body.issue_description,
        issue_modul: (!body.issue_modul) ? null : body.issue_modul,
        issue_menu: (!body.issue_menu) ? null : body.issue_menu,
        issue_patch: (!body.issue_patch) ? null : body.issue_patch,
        issue_status: body.issue_status,
        issue_notes: (!body.issue_notes) ? null : body.issue_notes,
        createdBy: user,
        updatedBy: null,
        createdAt: new Date(),
        updatedAt: null,
        deletedAt: null
    })

    issue.save()
        .then(result => {
            res.status(200).json({
                status: true,
                message: "issue is successfuly added"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
})

router.post('/update_status/:id', async(req, res, next) => {
    const body = req.body
    if (!(body.issue_status)) {
        res.status(400).send({
            status: false,
            message: 'issue_status are required'
        })
        return
    }

    const user = await User.findOne({ username: req.user.username }).select('_id username display_name')
    if (!user) {
        res.status(401).send({
            status: false,
            message: 'user not found, please relogin and get that token'
        })
        return
    }

    Issue.findOneAndUpdate({ _id: req.params.id }, {
            issue_status: (!body.issue_status) ? oldIssue.issue_status : body.issue_status,
            updatedBy: user,
            updatedAt: new Date(),
        }, { new: true })
        .then(result => {
            res.status(200).json({
                status: true,
                message: "issue is successfuly updated",
                data_updated: result
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
})

router.patch('/:id', async(req, res, next) => {
    const body = req.body

    const user = await User.findOne({ username: req.user.username }).select('_id username display_name')
    if (!user) {
        res.status(401).send({
            status: false,
            message: 'user not found, please relogin and get that token'
        })
        return
    }

    const oldIssue = await Issue.findOne({ _id: req.params.id })

    Issue.findOneAndUpdate({ _id: req.params.id }, {
            issue_type: (!body.issue_type) ? oldIssue.issue_type : body.issue_type,
            issue_description: (!body.issue_description) ? oldIssue.issue_description : body.issue_description,
            issue_modul: (!body.issue_modul) ? oldIssue.issue_modul : body.issue_modul,
            issue_menu: (!body.issue_menu) ? oldIssue.issue_menu : body.issue_menu,
            issue_patch: (!body.issue_patch) ? oldIssue.issue_patch : body.issue_patch,
            issue_status: (!body.issue_status) ? oldIssue.issue_status : body.issue_status,
            issue_notes: (!body.issue_notes) ? oldIssue.issue_notes : body.issue_notes,
            updatedBy: user,
            updatedAt: new Date(),
        }, { new: true })
        .then(result => {
            res.status(200).json({
                status: true,
                message: "issue is successfuly updated",
                data_updated: result
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
})

router.delete('/:id', async(req, res, next) => {

    if (!req.params.id) {
        res.status(404).send({
            status: false,
            message: 'not found'
        })
        return
    }

    const user = await User.findOne({ username: req.user.username }).select('_id username display_name')
    if (!user) {
        res.status(401).send({
            status: false,
            message: 'user not found, please relogin and get that token'
        })
        return
    }

    Issue.findOneAndUpdate({ _id: req.params.id }, {
            updatedBy: user,
            deletedAt: new Date(),
        }, { new: true })
        .then(result => {
            res.status(200).json({
                status: true,
                message: "issue is successfuly deleted",
                data_updated: result
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
})

module.exports = router