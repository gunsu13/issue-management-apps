const mongoose = require('mongoose');

const projectSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    project_name: {
        type: String,
        required: true
    },
    project_description: {
        type: String,
        required: true
    },
    project_client: {
        type: String,
        required: true
    },
    project_status: {
        type: String,
        required: true
    },
    createdBy: Object,
    updatedBy: Object,
    createdAt: mongoose.Schema.Types.Date,
    updatedAt: mongoose.Schema.Types.Date,
    deletedAt: mongoose.Schema.Types.Date
});

projectSchema.index({ project_name: 'text', project_description: 'text' });

module.exports = mongoose.model('projects', projectSchema);