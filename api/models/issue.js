const mongoose = require('mongoose');

const issueSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    project_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'projects'
    },
    issue_code: {
        type: String,
        required: true
    },
    issue_type: {
        type: String,
        required: true
    },
    issue_description: {
        type: String,
        required: true
    },
    issue_modul: String,
    issue_menu: String,
    issue_patch: String,
    issue_status: {
        type: String,
        required: true
    },
    issue_notes: String,
    createdBy: Object,
    updatedBy: Object,
    createdAt: mongoose.Schema.Types.Date,
    updatedAt: mongoose.Schema.Types.Date,
    deletedAt: mongoose.Schema.Types.Date
});

issueSchema.index({ issue_description: 'text', issue_type: 'text', issue_modul: 'text', issue_menu: 'text', issue_status: 'text' });

module.exports = mongoose.model('issues', issueSchema);