const mongoose = require('mongoose');

const commentSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    issue: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'issues'
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Users'
    },
    comment_body: {
        type: String,
        required: true
    },
    createdAt: mongoose.Schema.Types.Date,
    updatedAt: mongoose.Schema.Types.Date,
    deletedAt: mongoose.Schema.Types.Date
});

module.exports = mongoose.model('comments', commentSchema);