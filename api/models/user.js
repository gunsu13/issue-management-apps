const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    display_name: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true,
        index: true
    },
    password: {
        type: String,
        required: true
    },
    isAdmin: {
        type: String,
        required: true
    },
    token: String,
    createdAt: mongoose.Schema.Types.Date,
    deletedAt: mongoose.Schema.Types.Date
});

module.exports = mongoose.model('Users', userSchema);