const jwt = require("jsonwebtoken");

const config = require('../../config')

const verifyToken = (req, res, next) => {
    // only accept token on http headers
    const token = req.headers["x-access-token"];

    if (!token) {
        return res.status(403).json({
            message: "A token is required for authentication"
        });
    }

    try {
        const decoded = jwt.verify(token, config.secret);
        req.user = decoded;
    } catch (err) {
        return res.status(401).send({
            message: "Invalid Token / Token was expired"
        });
    }
    return next();
};

module.exports = verifyToken;