const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');
const config = require('./config')

// ambil route pada masing masing file
const verifyToken = require('./api/middleware/verifyJwt');
const commentRoutes = require('./api/routes/comment');
const userRoutes = require('./api/routes/user');
const projectRoutes = require('./api/routes/project');
const isuseRoutes = require('./api/routes/issue');
const authRoutes = require('./api/routes/auth');
// const officersRoutes = require('./api/routes/officers');

mongoose.connect(config.mongodb);

app.use(morgan('dev'));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// handle cors site access routes
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Authorization, x-access-token');

    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }

    next();
});

// declare routes pada middleware
// middleware routes
app.use('/users', verifyToken, userRoutes);
app.use('/projects', verifyToken, projectRoutes);
app.use('/issues', verifyToken, isuseRoutes);
app.use('/issues/comment', verifyToken, commentRoutes);
app.use('/auth', authRoutes);

// app.use('/officers', officersRoutes);

// handle error ketika tidak ada request yang cocok dengan routes
app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});

// tampilkan error pada response
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;